This project contains the MobilityLabs OpenApi documments for developers. Please, download the help repository and open index.html file for using the documentation.
Also you can test the system with the POSTMAN project added.
If you want to use the API methods, please, register your user in https://mobilitylabs.emtmadrid.es
For advanced uses API you may also register your App in the same portal.
Please, be careful with your credentials. We recommended to use the ClientId and passKey params  in order to preserve mail and password

We apreciate your comments in out portal and your participation in forums. Also you can your own data in order to shared with other people.
Thanks for your contribute to foment the mobility.
 
Enjoy the experience of opendata portals.
The Mobilitylabs Madrid team.

